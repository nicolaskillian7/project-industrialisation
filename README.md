Nicolas-Killian Duchassin
Je livre sur l'ancien serveur de prod.

Architecture :

**index.html**, la frontpage du site

**.gitlab-ci.yml**, fichier qui définis la structure et l'ordre des pipelines

**package.json**, fichier permettant d'installer toutes les dépendances

**Assets/** à l'intérieur se trouve le fichier simple.less et simple.css qui est généré lors du build

**src/** à l'intérieur se trouve index.js le fichier où il y a les fonctionnalités js

**test/** à l'intérieur se trouve index.test.js, c'est dans ce fichier que les tests sont exécutés

Explication du **.gitlab-ci.yml** :
Ce fichier permet qu'à chaque commit ou push, on déclanche des pipelines d'intégration continue et de livraison continues.

stages - Opération permettant de grouper les jobs suivant :

  - tests, on lance une npm install, une commande qui installe les dependencies, puis on lance les tests
  - build, on installe less, puis on l'execute pour générer un fichier css à partir du fichier less
  - deploy, c'est le stage de déploiment du master sur le serveur de prod

Explications sur less :

Less est un langage permettant de faire du css tout en ajoutant des fonctionnalités. Mais les navigateurs ne comprennent que le langage css.
Par conséquent il faut donc générer un fichier css à partir d'un fichier less.
